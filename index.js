//JSON -> JavaScript Object Notation -> easy to read and write


//JSON Objects -> use the "key/value pairs" just like the object properties in JS.

//"Key/properties" are enclose with double quote ("").

/* 

    Syntax:
    {
        "propertyA" : "valueA",
        "propertyB" : "valueB"
    }

*/

/* "cities" ; [
{

    "city" : "Quezon City",
    "province" : "Metro Manila",
    "country" : "Philippines"

},
{

    "city" : "Manila City",
    "province" : "Metro Manila",
    "country" : "Philippines"

}
]; */

//JSON Methods
//The "JSON Object"-> contains methods for parsing and converting data into stringified JSON.


//Stringified Data -> Conversion

let batchesArr = [{
        batchName: "Batch 203",
        schedule: "Full Time",
    },
    {
        batchName: "Batch 204",
        schedule: "Part Time",
    }
]

console.log(batchesArr);

//Stringfy
console.log("Result from stringify method: ");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
    name: "John",
    age: 31,
    address: {
        city: "Manila",
        country: "Philippines"
    }
});

console.log(data);

/* let firstName = prompt("Enter your first name: ");
let lastName = prompt("Enter your last name: ");
let email = prompt("Enter your email: ");
let password = prompt("Enter your password: ");

let otherData = JSON.stringify({
    firstName: firstName,
    lastName: lastName,
    email: email,
    password: password
});

console.log(otherData); */

//Converting Stringified JSON into JavaScript Objects
let batchesJSON = `[{
        "batchName" : "Batch 203",
        "schedule" : "Full Time"
    },
    {
        "batchName" : "Batch 204",
        "schedule" : "Part Time"
    }
]`
console.log("Batches JSON Content: ");
console.log(batchesJSON);

//JSON.parse method to convers JSON Object to JavaScript Objects
console.log("Result from parse method:");

//console.log(JSON.parse(batchesJSON));

let praseBatches = JSON.parse(batchesJSON);
console.log(praseBatches[0].batchName);

let stringifiedObject = `{
    "name":"John",
    "age":31,
    "address":
    {
        "city":"Manila",
        "country":
        "Philippines"}
}`

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));